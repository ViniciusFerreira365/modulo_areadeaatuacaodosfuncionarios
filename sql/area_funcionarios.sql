-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 23-Jun-2019 às 02:54
-- Versão do servidor: 10.1.40-MariaDB
-- versão do PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2_modulo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `area_funcionarios`
--

CREATE TABLE `area_funcionarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `posicao` varchar(200) NOT NULL,
  `escritorio` varchar(200) NOT NULL,
  `idade` varchar(2) NOT NULL,
  `data_inicio` varchar(10) NOT NULL,
  `salario` int(12) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `area_funcionarios`
--

INSERT INTO `area_funcionarios` (`id`, `nome`, `posicao`, `escritorio`, `idade`, `data_inicio`, `salario`, `last_modified`) VALUES
(1, 'Vinicius', 'CEO', 'Geral', '34', '22/04/68', 10000, '2019-06-22 21:22:26'),
(2, 'Hamilton', 'Gerente', 'Contabeis', '45', '22/04/68', 4500, '2019-06-22 21:22:38'),
(5, 'lucas', 'Faxineiro', 'Recepção', '22', '22/04/68', 1500, '2019-06-22 21:24:19'),
(6, 'Jessica', 'Executiva', 'Relações EXT', '29', '22/04/68', 6450, '2019-06-22 21:25:46'),
(7, 'Fatima', 'Comissionada', 'Escritorio N°2', '54', '11/12/2045', 3690, '2019-06-22 21:26:35'),
(8, 'Thiago', 'Supervisor', 'T.I', '34', '23/05/2021', 5000, '2019-06-22 21:27:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area_funcionarios`
--
ALTER TABLE `area_funcionarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area_funcionarios`
--
ALTER TABLE `area_funcionarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
