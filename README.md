# AT03 - HMVC Modulo de Funcionario(Especialidades)

**Este repositório tem como proposito o atividade 3 de LP2, onde é necessário criar um modulo designado ao aluno(Especialidade/Área de atuação de funcionários), este modulo foi pensado para uma empresa que queira cadastrar, editar ou deletar  seus funcionários do sistema de forma simples e direta, um modulo simples e robusto com validador e testes de unidade. O modulo foi criado a partir do código-base dado pelo professor. Minha idéia fosse criar um modulo onde fosse capaz de ser um simples cadastro geral para ser utilizado em outros programas apenas mudando formularios e algumas variaveis, existem alguns problemas que vão ser resolvidos com mais tempo e estudo.**


---

## Executar o modulo e testa-lo
* Clone este repositório em um diretório chamado: **modulo_areadeaatuacaodosfuncionarios**
* Importe para o MySQL o arquivo area_funcionarios.sql que se encontra na pasta sql
* Digite no navegador **http://localhost/modulo_areadeaatuacaodosfuncionarios/Funcionario**
* Digite no navegador **http://localhost/modulo_areadeaatuacaodosfuncionarios/Funcionario/cadastro** para cadastrar um novo funcionario
* Utlize os icones para editar e excluir um funcionario.

---
## Executar os testes de unidade do módulo
* Os testes estao localizados em /controllers/test
* No módulo tarefa existe um teste chamado TurmaTest; execute-o acessando **http://localhost/modulo_areadeaatuacaodosfuncionarios/Funcionario/test/all**

---
## **Importante**
* Nao foi possivel fazer o teste de regreção e aceitação.
* Todo o codigo está documentado e comentado nota-se que os teste de unidade funcionam parcialmente
* O módulo funciona perfeitamente ele funciona como um cadastro de usuario(funcionario) onde é
possivel editar nome,salario,cadastro,posição e mais.
---

## Alguns snapshorts do codigo

~~~php
class FuncionarioModel extends MY_Controller
{
    /** *
     * Pega o Id dos funcionarios
     * e instancia uma variavel to tipo
     * @user
     *@return int id getByid
    */
    public function carrega_funcionario($id)
    {
        $user = new User();
        return $user->getById($id);

    }
~~~~

~~~php
    class FuncionarioValidator extends Validator{
    public function form_cadastro(){
        $this->form_validation->set_rules('nome', 'Nome do Usuario', 'required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('posicao', 'posição na empresa', 'required|min_length[2]|max_length[200]');
        $this->form_validation->set_rules('escritorio', 'Escritorio no funcionario', 'required|min_length[2]|max_length[200]');
        $this->form_validation->set_rules('idade', 'idade', 'trim|required|exact_length[2]');
        $this->form_validation->set_rules('data_inicio', 'Data de Inicio', 'trim|required|exact_length[10]');
        $this->form_validation->set_rules('salario', 'salario', 'required|min_length[2]|max_length[12]');
        return $this->form_validation->run();
    }
~~~

~~~php
    class User
{
    /** *
     *  Criação das variaveis que
     * serão usadas
     */
    private $nome;
    private $posicao;
    private $escritorio;
    private $idade;
    private $data_inicio;
    private $salario;
~~~
## **Informações da conta caso nao baixe o modulo**
*  **https://bitbucket.org/ViniciusFerreira365/modulo_areadeaatuacaodosfuncionarios/src/master/** acessar a pagina inicial do repositorio
* **git clone https://ViniciusFerreira365@bitbucket.org/ViniciusFerreira365/modulo_areadeaatuacaodosfuncionarios.git** comando no git em HTTPS

## Atenciosamente Vinicius Ferreira
### **Prontuario: GU3002519.**
###  **Email: v.ferreira@aluno.ifsp.edu.br**
