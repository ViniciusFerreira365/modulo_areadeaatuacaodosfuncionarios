<?php

class User
{
    /** *
     *  Criação das variaveis que
     * serão usadas
     */
    private $nome;
    private $posicao;
    private $escritorio;
    private $idade;
    private $data_inicio;
    private $salario;

    private $db;

    /** *
     *  Passa as váriaveis para o construtor
     * e instancia variavies do bando e do
     * codeigniter
     */
    public function __construct($nome = null, $posicao = null, $escritorio = null, $idade = null, $data_inicio = null, $salario = null)
    {
        $this->nome = $nome;
        $this->posicao = $posicao;
        $this->escritorio = $escritorio;
        $this->idade = $idade;
        $this->data_inicio = $data_inicio;
        $this->salario = $salario;

        $ci = &get_instance();
        $this->db = $ci->db;

    }

    /** *
     *  Salva valores no banco
     *@return  sql    */
    public function save()
    {
        $sql = "INSERT INTO area_funcionarios (nome,posicao,escritorio,idade,data_inicio,salario) VALUES ('$this->nome','$this->posicao','$this->escritorio','$this->idade','$this->data_inicio','$this->salario')";
        $this->db->query($sql); //Executo variavel sql
    }

    /** *
     *  Obtem a lista de todos os usuarios cadastrados
     *@return associative array
     */
    public function getAll()
    { //lê tudo que está no banco
        $sql = "SELECT * FROM area_funcionarios";
        $res = $this->db->query($sql); //resultado
        return $res->result_array(); //Voltar dados em um vetor
    }

    /** *
     *  Obtem o Id de todos os usuarios cadastrados
     *@return int id
     */
    public function getById($id)
    {
        $rs = $this->db->get_where('area_funcionarios', "id = $id");
        return $rs->row_array(); //Devolve a linha sem precisar fazer uma matriz quando o resultado é unico
    }

    /** *
     *  Atualiza dados cadastrados no banco
     *@return int id, date
     */
    public function update($data, $id)
    {
        $this->db->update('area_funcionarios', $data, "id = $id");
        echo $this->db->last_query();
        return $this->db->affected_rows();
    }
/** *
 *  Deleta funcionario do banco
 *@return int id
 */
    public function delete($id)
    {
        $this->db->delete('area_funcionarios', "id = $id");
    }

}
