<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Classe para o FuncionarioValidator
 * @param int id como parametro*/
abstract class Validator extends CI_Object
{

    abstract public function getData();
    abstract public function validate();
}
