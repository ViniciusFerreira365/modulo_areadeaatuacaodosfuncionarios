<?php
include_once APPPATH . 'Validator.php';
/**
 * Valitador do cadastro de usuario
 * deve seguir estes parametros
 * */
class FuncionarioValidator extends Validator
{
    public function form_cadastro()
    {
        $this->form_validation->set_rules('nome', 'Nome do Usuario', 'required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('posicao', 'posição na empresa', 'required|min_length[2]|max_length[200]');
        $this->form_validation->set_rules('escritorio', 'Escritorio no funcionario', 'required|min_length[2]|max_length[200]');
        $this->form_validation->set_rules('idade', 'idade', 'trim|required|exact_length[2]');
        $this->form_validation->set_rules('data_inicio', 'Data de Inicio', 'trim|required|exact_length[10]');
        $this->form_validation->set_rules('salario', 'salario', 'required|min_length[2]|max_length[12]');
        return $this->form_validation->run();
    }
    /**
     * Funcao retorna dados para o vetor data
     * @associative array $data como parametro*/
    public function getData()
    {
        $data['nome'] = $this->input->post('nome');
        $data['posicao'] = $this->input->post('posicao');
        $data['escritorio'] = $this->input->post('escritorio');
        $data['idade'] = $this->input->post('idade');
        $data['data_inicio'] = $this->input->post('data_inicio');
        $data['salario'] = $this->input->post('salario');
        return $data;
    }

}
