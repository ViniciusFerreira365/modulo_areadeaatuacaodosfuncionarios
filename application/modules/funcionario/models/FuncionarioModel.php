<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once APPPATH . 'modules/Funcionario/libraries/User.php';

class FuncionarioModel extends MY_Controller
{
    /** *
     * Pega o Id dos funcionarios
     * e instancia uma variavel to tipo
     * @user
     *@return int id getByid
     */
    public function carrega_funcionario($id)
    {
        $user = new User();
        return $user->getById($id);

    }

    /** *
     *  Cria os dados no banco
     * passando os inputs e
     * retornando na função save do tipo user
     *@return user user
     */
    public function criar()
    {
        if (sizeof($_POST) == 0) {
            return;
        }

        $nome = $this->input->post('nome');
        $posicao = $this->input->post('posicao');
        $escritorio = $this->input->post('escritorio');
        $idade = $this->input->post('idade');
        $data_inicio = $this->input->post('data_inicio');
        $salario = $this->input->post('salario');

        $user = new User($nome, $posicao, $escritorio, $idade, $data_inicio, $salario);
        $user->save();
    }
/** *
 *  Lista os dados através
 * de um @foreach com o vetor $data e $row
 * com as funçoes dos botoes editar e deletar
 *@return int id
 */
    public function lista()
    {
        $html = '';
        $user = new User();
        $data = $user->getAll();
        $html .= '<table class  = "table">';
        foreach ($data as $row) {
            $html .= '<tr>';
            $html .= '<td>' . $row['nome'] . '</td>';
            $html .= '<td>' . $row['posicao'] . '</td>';
            $html .= '<td>' . $row['escritorio'] . '</td>';
            $html .= '<td>' . $row['idade'] . '</td>';
            $html .= '<td>' . $row['data_inicio'] . '</td>';
            $html .= '<td>' . $row['salario'] . '</td>';
            $html .= '<td>' . $this->get_edit_icon($row['id']) . '</td></tr>';
        }
        $html .= '</table>';
        return $html;

    }
/** *
 *  OFunção de edição, remoção de
 * dados da tabela pelo id
 *@return int id
 */
    private function get_edit_icon($id)
    {
        $html = '';
        $html .= '<a href="' . base_url('/funcionario/edit/' . $id) . '"><i class="fas fa-edit mr-3 text-info"></i>';
        $html .= '  <a href="' . base_url('/funcionario/delete/' . $id) . '"><i class="fas fa-times ml-3 text-danger"></i>';
        return $html;
    }

    /** *
     *  Atualiza(update) o dado alterado na tabela
     *@return int id
     */
    public function atualizar($id)
    {
        if (sizeof($_POST) == 0) {
            return;
        }

        $data = $this->input->post();
        $user = new User();

        if ($user->update($data, $id)) {
            redirect('Funcionario');
        }

    }

/** *
 *  Deleta um usuario pelo id
 *@return int id
 */
    public function delete($id)
    {
        $user = new User();
        $user->delete($id);
    }

}
