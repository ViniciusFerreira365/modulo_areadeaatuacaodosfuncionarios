<!--Formulario para a inserção dos dados -->
<div class="container">
    <div class="row mt-4">
        <div class="col-md-4 mx-auto">
            <form method = "POST" class="text-center border border-light p-5">
            <p class = "h4 mb-4"> <?=$titulo?></p>
            <div class="form-row mb-4">
            <input type="text" value = "<?=isset($user['nome']) ? $user['nome'] : ''?>" name = "nome" id="nome" class="form-control" placeholder="Nome">
            <input type="text" value = "<?=isset($user['posicao']) ? $user['posicao'] : ''?>" name = "posicao" id="posicao" class="form-control" placeholder="posicao">
            <input type="text" value = "<?=isset($user['escritorio']) ? $user['escritorio'] : ''?>" name = "escritorio" id="escritorio" class="form-control mb-4" placeholder="Escritorio">
            <input type="text" value = "<?=isset($user['idade']) ? $user['idade'] : ''?>" name = "idade" id="idade" class="form-control" placeholder="idade">
            <input type="text" value = "<?=isset($user['data_inicio']) ? $user['data_inicio'] : ''?>" name = "data_inicio" id="data_inicio" class="form-control" placeholder="Data inicio">
            <input type="text" value = "<?=isset($user['salario']) ? $user['salario'] : ''?>" name = "salario" id="salario" class="form-control" placeholder="salario">
            </div>
            <button class="btn btn-info my-4 btn-block" type="submit"><?=$acao?></button>
            </form>
        </div>
    </div>
</div>

