<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/funcionario/libraries/User.php';
include_once APPPATH . 'modules/funcionario/models/FuncionarioModel.php';
include_once APPPATH . 'modules/funcionario/controllers/test/builder/FuncionarioDataBuilder.php';
/**
 * teste para limpar a tabela
 * e criar o construtor pai
 */
class E2ETest extends Toast{

    function __construct(){
        parent::__construct('E2E Test');
    }

    function test_limpa_tabela_de_teste(){
        $builder = new FuncionarioDataBuilder('lp2_modulo');
        $builder->clean_table();

        $funcionario = new FuncionarioModel ();
        
    }

}