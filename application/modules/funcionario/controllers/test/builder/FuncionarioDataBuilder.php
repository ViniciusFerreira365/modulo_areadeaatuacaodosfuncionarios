<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class FuncionarioDataBuilder extends TestDataBuilder {
    /**
     * teste Construtor passando dados para teste */

    public function __construct($table = 'lp2_modulo_test'){
        parent::__construct('area_funcionarios', $table);
    }

    function getData($index = 1){
        $data[9]['nome'] = 'Vinicius';
        $data[9]['posicao'] = 'CEO';
        $data[9]['escritorio'] = 'RH';
        $data[9]['idade'] = '22';
        $data[9]['data_inicio'] = '22/04/68';
        $data[9]['salario'] = '10000';

        $data[10]['nome'] = 'Hamilton';
        $data[10]['posicao'] = 'Gerente';
        $data[10]['escritorio'] = 'Contabeis';
        $data[10]['idade'] = '34';
        $data[10]['data_inicio'] = '22/04/68';
        $data[10]['salario'] = '4500';

        $data[11]['nome'] = 'Lucas';
        $data[11]['posicao'] = 'Faxineiro';
        $data[11]['escritorio'] = 'Geral';
        $data[11]['idade'] = '45';
        $data[11]['data_inicio'] = '22/04/68';
        $data[11]['salario'] = '4500';
        return $index > 1 ? $data[$index] : $data;
    }

}