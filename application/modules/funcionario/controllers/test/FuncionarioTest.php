<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/funcionario/libraries/User.php';
include_once APPPATH . 'modules/funcionario/models/FuncionarioModel.php';
include_once APPPATH . 'modules/funcionario/controllers/test/builder/FuncionarioDataBuilder.php';

/**
 * Teste de unidade padrao
 */
class FuncionarioTest extends Toast{
    private $builder;
    private $tarefa;

    function __construct(){
        parent::__construct('FuncionarioTest');
    }

    function _pre(){
        $this->builder = new FuncionarioDataBuilder();
        $this->funcionario = new FuncionarioModel();
    }

    // apenas ilustrativo
    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->tarefa, "Erro na criação da tarefa");
    }

    // apenas ilustrativo
    function test_selecionado_banco_de_teste(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_modulo_test', $s, 'Erro na seleção do banco de teste');
    }

    function test_insere_registro_na_tabela(){
        // cenário 1: vetor com dados corretos
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        $id1 = $this->tarefa->insert($user);
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        // verificação: o objeto criado é, de fato, aquele que enviamos?
        $task = $this->tarefa->get(array('id' => 1))[0];
        $this->_assert_equals($data['nome'], $task['nome']);
        $this->_assert_equals($data['posicao'], $task['posicao']);
        $this->_assert_equals($data['escritorio'], $task['escritorio']);
        $this->_assert_equals($data['idade'], $task['idade']);
        $this->_assert_equals($data['data_inicio'], $task['data_inicio']);
        $this->_assert_equals($data['salario'], $task['salario']);

        // cenário 2: vetor vazio
        $id2 = $this->tarefa->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

        // cenário 3: vetor com dados inesperados
        $info = $this->builder->getData(1);
        $info['unexpected_col_name'] = 1;
        $id = $this->tarefa->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        // cenário 4: vetor com dados incompletos... deve ser
        // tratado pela validação, mas tem que ser pensado aqui
        $v = array('titulo' => 'vetor incompleto');
        $id = $this->tarefa->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");
    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $tasks = $this->tarefa->get();
        $this->_assert_equals(3, sizeof($tasks), "Número de registros incorreto");
    }

    function test_carrega_registro_condicionalmente(){
        $this->builder->clean_table();
        $this->builder->build();

        $task = $this->tarefa->get(array('nome' => 'vinicius', 'posicao' => 'CEO'))[0];
        $this->_assert_equals('vinicus', $task['nome'], "Erro no nome");
        $this->_assert_equals('CEO', $task['posicao'], "Erro na posicao");
    }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        // lê um registro do bd
        $task1 = $this->tarefa->get(array('id' => 2))[0];
        $this->_assert_equals('posicao', $task1['posicao'], "Erro na posicao");
        $this->_assert_equals('contabeis', $task1['escritorio'], "Erro no ID do escritorio");

        // atualiza seus valores
        $task1['posicao'] = 'CEO';
        $task1['escritorio'] = 'RH';
        $this->tarefa->insert_or_update($task1);

        // lê novamente, o mesmo objeto, e verifica se foi atualizado
        $task2 = $this->tarefa->get(array('id' => 2))[0];
        $this->_assert_equals($task1['posicao'], $task2['posicao'], "Erro no posicao");
        $this->_assert_equals($task1['escritorio'], $task2['escritorio'], "Erro no id escritorio");
    }
    
    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        // verifica que o registro existe
        $task1 = $this->tarefa->get(array('id' => 2))[0];
        $this->_assert_equals('CEO', $task1['posicao'], "Erro no posicao");
        $this->_assert_equals('RH', $task1['escritorio'], "Erro no id escritorio");

        // remove o registro
        $this->tarefa->delete(array('id' => 2));

        // verifica que o registro não existe mais
        $task2 = $this->tarefa->get(array('id' => 2));
        $this->_assert_equals_strict(0, sizeof($task2), "Registro não foi removido");
    }

}