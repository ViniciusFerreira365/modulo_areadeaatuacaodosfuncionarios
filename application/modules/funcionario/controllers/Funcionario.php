<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Funcionario extends MY_Controller
{
/**Funcao principal */
    public function index()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('FuncionarioModel', 'model');

        /**
         * Lista todos os dados passando
         * @array v como*/
        $v['lista'] = $this->model->lista();
        $this->load->view('table_view', $v);
        $this->load->view('common/footer');
    }
/**
 * Cadastra dados na tabela
 * utilizando o model*/
    public function cadastro()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $data['acao'] = "Enviar";
        $data['titulo'] = "Cadastro de Usuario";

        $this->load->model('FuncionarioModel', 'model');
        $this->model->criar();

        $this->load->view('form_cadastro', $data);
        $this->load->view('common/footer');

    }
/**
 * Edita os dados da tabela
 * @param int id como parametro*/
    public function edit($id)
    {
        $this->load->view('common/header');

        $this->load->model('FuncionarioModel', 'model');
        $this->model->atualizar($id);

        $data['titulo'] = "Edição de Usuario";
        $data['acao'] = "Atualizar";
        $data['user'] = $this->model->carrega_funcionario($id);

        $this->load->view('form_cadastro', $data);
        $this->load->view('common/footer');

    }
/**
 * Deleta os dados da tabela e redireciona a pagina
 * @param int id como parametro*/
    public function delete($id)
    {
        $this->load->model('FuncionarioModel', 'model');
        $this->model->delete($id);
        redirect('');

    }

}
